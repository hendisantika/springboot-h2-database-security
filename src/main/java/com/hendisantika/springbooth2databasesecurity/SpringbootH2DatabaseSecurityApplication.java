package com.hendisantika.springbooth2databasesecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootH2DatabaseSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootH2DatabaseSecurityApplication.class, args);
    }

}
