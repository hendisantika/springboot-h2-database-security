# Spring Boot + Spring Security + H2 Database

In this repository, we will see how to secure your web application by authenticating and authorizing users against database. 
User and role information is stored in database.

## Tools :
* Spring Boot 2.2.6.RELEASE
* Spring MVC
* Spring Security
* Maven
* IntelliJ IDEA
* Lombok

## Things to do list:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/springboot-h2-database-security.git`
2. Go to the folder: `cd springboot-h2-database-security`
3. Run the application: `mvn clean spring-boot:run` and
4. Navigate to H2 database console by navigating to http://localhost:8081/h2-console
5. Once the application is up and running, H2 database will be automatically started. You can navigate to http://localhost:8081/h2-console to see the tables created as shown below-
6. Now go to application http://localhost:8080/

## Screen shot

H2 Login

![H2 Login](img/h2-login.png "H2 Login")

H2 Tables

![H2 Tables](img/h2.png "H2 Tables")

Registration Page

![Registration Page](img/registration.png "Registration Page")

Login Page

![Login Page](img/login.png "Login Page")

Admin Page

![Admin Page](img/admin.png "Admin Page")
